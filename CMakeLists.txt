# KAO1Unpack
# Copyright (C) 2022 mrkubax10
 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

project(KAO1Unpack C)
cmake_minimum_required(VERSION 3.10)

add_executable(KAO1Unpack
	src/main.c
	src/binary_utils.c
	src/pak.c
	src/fs.c
	src/str.c
	src/adpcm.c
	src/encoding.c
	src/mode_unpack.c
	src/mode_pack.c
	src/mode_list.c
	src/mode_rename.c
	src/mode_add.c
	src/mode_delete.c
	src/mode_replace.c
	src/mode_unpack_selected.c
	src/mode_stat.c
)

target_include_directories(KAO1Unpack PRIVATE "${CMAKE_SOURCE_DIR}")
