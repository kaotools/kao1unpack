// KAO1Unpack
// Copyright (C) 2023 mrkubax10

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "mode_rename.h"

#include "pak.h"

void modeRename(const char* input,const char* from,const char* to){
	Pak pak;
	Pak_init(&pak);
	if(!Pak_load(&pak,input))
		return;

	if(Pak_renameFile(&pak,from,to))
		printf("%s -> %s\n",from,to);
	else
		printf("Failed to rename file %s\n",from);

	Pak_delete(&pak);
}
