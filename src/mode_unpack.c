// KAO1Unpack
// Copyright (C) 2023 mrkubax10

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "mode_unpack.h"

#include "pak.h"
#include "fs.h"
#include "str.h"

void modeUnpack(const char* input,const char* output,bool convertADPCM,bool convertDEF){
	Pak pak;
	Pak_init(&pak);
	if(!Pak_load(&pak,input))
		return;

	char temp[81];
	for(unsigned i=0; i<pak.fileCount; i++){
		PakFile* header=&pak.fileHeaders[i];
		stringReplace(header->filename,'\\','/');
		snprintf(temp,81,"%s/%s",output,header->filename);
		createFolders(temp);
		printf("[%d/%d] Extracting file '%s' at offset %d with size %d...\n",i+1,pak.fileCount,header->filename,header->offset,header->size);
		fflush(stdout);
		Pak_extractFileByIndex(&pak,i,temp,convertADPCM,convertDEF);
	}

	Pak_delete(&pak);
}
