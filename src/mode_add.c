// KAO1Unpack
// Copyright (C) 2023 mrkubax10

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "mode_add.h"

#include <stdlib.h>
#include <string.h>

#include "pak.h"
#include "str.h"
#include "encoding.h"

void modeAdd(const char* input,const char* filename,const char* importedFilename,bool convertADPCM,bool convertDEF){
	Pak pak;
	Pak_init(&pak);
	if(!Pak_load(&pak,input))
		return;

	FILE* inp=fopen(filename,"rb");
	if(!inp){
		printf("Failed to open file %s for reading\n",filename);
		Pak_delete(&pak);
		return;
	}
	fseek(inp,0,SEEK_END);
	uint32_t size=ftell(inp);
	rewind(inp);
	char* data=malloc(size);
	fread(data,size,1,inp);
	fclose(inp);

	if((stringEndsWith(filename,".def") || stringEndsWith(filename,".at") || stringStartsWith(filename,"text/localize.win/denis.") || stringEndsWith(filename,"/scene.cut")) && convertDEF)
		encodeDEFFile(data,size);
	else if(stringEndsWith(input,".adp") && strncmp(data,"RIFF",4)==0 && convertADPCM){
		char* convertedData=encodeADPCMFile(filename,data,size,&size);
		if(convertedData){
			free(data);
			data=convertedData;
		}
	}

	printf("Writing %s at offset %d with size %d...\n",filename,pak.contentSize,size);
	Pak_addFile(&pak,importedFilename,data,size);

	free(data);
	Pak_delete(&pak);
}
