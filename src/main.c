// KAO1Unpack
// Copyright (C) 2022 mrkubax10

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include <stdlib.h>

#include "mode_unpack.h"
#include "mode_pack.h"
#include "mode_list.h"
#include "mode_rename.h"
#include "mode_add.h"
#include "mode_delete.h"
#include "mode_replace.h"
#include "mode_unpack_selected.h"
#include "mode_stat.h"

typedef enum Mode{
	MODE_UNPACK,MODE_PACK,MODE_LIST,MODE_RENAME,MODE_ADD,MODE_DELETE,MODE_REPLACE,MODE_UNPACK_SELECTED,MODE_STAT
} Mode;
static void printUsage(char** args){
	printf("KAO1Unpack 2.0.0\n");
	printf("Usage: %s <arguments...>\n",args[0]);
	printf("--output <file/folder>\t\t\tSet output to <file/folder>\n");
	printf("--input <file/folder>\t\t\tSet input to <file/folder>\n");
	printf("--unpack\t\t\t\tSet archive unpacking mode\n");
	printf("--pack\t\t\t\t\tSet archive packing mode\n");
	printf("--list\t\t\t\t\tList files in archive\n");
	printf("--rename <from> <to>\t\t\tRename file <from> to <to>\n");
	printf("--add <file> <filename>\t\t\tAdd file to archive\n");
	printf("--delete <file>\t\t\t\tDelete file from archive\n");
	printf("--replace <filename> <file>\t\tReplace file in archive\n");
	printf("--unpack-selected <filenames...>\tSet archive unpacking mode but only unpack specified files (separated by ';')\n");
	printf("--stat\t\t\t\t\tShow archive statistics\n");
	printf("--convert-adpcm\t\t\t\tConvert ADPCM to WAV or WAV to ADPCM\n");
	printf("--convert-def\t\t\t\tDecode or encode DEF\n");
	printf("--use-windows-path\t\t\tUse \\ as directory separator when in pack mode\n");
	printf("--help\t\t\t\t\tDisplay this info\n");
}
static char* defaultOutput(char* input){
	unsigned length=strlen(input)+5; // + _out + null terminator
	char* newOutput=malloc(length);
	snprintf(newOutput,length,"%s_out",input);
	return newOutput;
}

int main(int argc,char** args){
	if(argc<3){
		printUsage(args);
		return -1;
	}
	Mode mode;
	char* output=0;
	char* input=0;
	char* renameFrom=0;
	char* renameTo=0;
	char* addFile=0;
	char* deleteName=0;
	char* addImportedFilename=0;
	char* replaceFilename=0;
	char* replaceFile=0;
	char* unpackSelectedList=0;
	bool stateConvertADPCM=false;
	bool stateConvertDEF=false;
	bool stateUseWindowsPath=false;
	for(int i=1; i<argc; i++){
		if(strcmp(args[i],"--input")==0){
			if(++i>=argc){
				printf("--input requires input file\n");
				return 1;
			}
			input=args[i];
			if(output && strcmp(input,output)==0){
				printf("Input cannot be the same as output\n");
				return 1;
			}
		}
		else if(strcmp(args[i],"--output")==0){
			if(++i>=argc){
				printf("--output requires input file\n");
				return 1;
			}
			output=args[i];
			if(input && strcmp(output,input)==0){
				printf("Output cannot be the same as input\n");
				return 1;
			}
		}
		else if(strcmp(args[i],"--unpack")==0){
			mode=MODE_UNPACK;
		}
		else if(strcmp(args[i],"--pack")==0){
			mode=MODE_PACK;
		}
		else if(strcmp(args[i],"--list")==0){
			mode=MODE_LIST;
		}
		else if(strcmp(args[i],"--rename")==0){
			if(++i>=argc){
				printf("--rename requires source filename\n");
				return 1;
			}
			renameFrom=args[i];
			if(++i>=argc){
				printf("--rename requires destination filename\n");
				return 1;
			}
			renameTo=args[i];
			mode=MODE_RENAME;
		}
		else if(strcmp(args[i],"--add")==0){
			if(++i>=argc){
				printf("--add requires file to add\n");
				return 1;
			}
			addFile=args[i];
			if(++i>=argc){
				printf("--add requires filename\n");
				return 1;
			}
			addImportedFilename=args[i];
			mode=MODE_ADD;
		}
		else if(strcmp(args[i],"--delete")==0){
			if(++i>=argc){
				printf("--delete requires file to delete\n");
				return 1;
			}
			deleteName=args[i];
			mode=MODE_DELETE;
		}
		else if(strcmp(args[i],"--replace")==0){
			if(++i>=argc){
				printf("--replace requires replaced filename\n");
				return 1;
			}
			replaceFilename=args[i];
			if(++i>=argc){
				printf("--replace requires file to replace\n");
				return 1;
			}
			replaceFile=args[i];
			mode=MODE_REPLACE;
		}
		else if(strcmp(args[i],"--unpack-selected")==0){
			if(++i>=argc){
				printf("--unpack-selected requires filename list\n");
				return 1;
			}
			unpackSelectedList=args[i];
			mode=MODE_UNPACK_SELECTED;
		}
		else if(strcmp(args[i],"--stat")==0)
			mode=MODE_STAT;
		else if(strcmp(args[i],"--convert-adpcm")==0){
			stateConvertADPCM=true;
		}
		else if(strcmp(args[i],"--convert-def")==0){
			stateConvertDEF=true;
		}
		else if(strcmp(args[i],"--use-windows-path")==0){
			stateUseWindowsPath=true;
		}
		else if(strcmp(args[i],"--help")==0){
			printUsage(args);
			return 0;
		}
		else{
			printf("Invalid argument %s\n",args[i]);
			return 1;
		}
	}
	if(!input){
		printf("--input argument is required\n");
		return 1;
	}
	switch(mode){
	case MODE_UNPACK:{
		char* newOutput=0;
		if(!output)
			newOutput=defaultOutput(input);
		modeUnpack(input,newOutput?newOutput:output,stateConvertADPCM,stateConvertDEF);
		if(newOutput)
			free(newOutput);
		break;
	}
	case MODE_PACK:{
		char* newOutput=0;
		if(!output)
			newOutput=defaultOutput(input);
		modePack(input,newOutput?newOutput:output,stateConvertADPCM,stateConvertDEF,stateUseWindowsPath);
		if(newOutput)
			free(newOutput);
		break;
	}
	case MODE_LIST:
		modeList(input);
		break;
	case MODE_RENAME:
		modeRename(input,renameFrom,renameTo);
		break;
	case MODE_ADD:
		modeAdd(input,addFile,addImportedFilename,stateConvertADPCM,stateConvertDEF);
		break;
	case MODE_DELETE:
		modeDelete(input,deleteName);
		break;
	case MODE_REPLACE:
		modeReplace(input,replaceFilename,replaceFile,stateConvertADPCM,stateConvertDEF);
		break;
	case MODE_UNPACK_SELECTED:{
		char* newOutput=0;
		if(!output)
			newOutput=defaultOutput(input);
		modeUnpackSelected(input,newOutput?newOutput:output,unpackSelectedList,stateConvertADPCM,stateConvertDEF);
		if(newOutput)
			free(newOutput);
		break;
	}
	case MODE_STAT:
		modeStat(input);
	}
	return 0;
}
