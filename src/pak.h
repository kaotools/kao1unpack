// KAO1Unpack
// Copyright (C) 2022 mrkubax10
// Thanks to Flower35 for PAK documentation: https://drive.google.com/file/d/1jloDqtUak-9OhOQ6Ayflxj2rdGT5HqZq/view

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef SRC_PAK_H
#define SRC_PAK_H

#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>

typedef struct PakFile{
	char filename[80];
	uint32_t offset;
	uint32_t size;
} PakFile;
typedef struct Pak{
	PakFile* fileHeaders;
	uint32_t fileCount;
	uint32_t contentSize;
	long size;
	FILE* file;
} Pak;
void Pak_init(Pak* self);
void Pak_delete(Pak* self);
PakFile* Pak_lookupFile(Pak* self,const char* filename,unsigned* index);
bool Pak_load(Pak* self,const char* path);
bool Pak_loadEntryTable(Pak* self,const char* path);
char* Pak_getFile(Pak* self,const char* filename,uint32_t* size);
char* Pak_getFileByIndex(Pak* self,unsigned index,uint32_t* size);
void Pak_addFile(Pak* self,const char* filename,const char* fileData,uint32_t fileSize);
bool Pak_deleteFile(Pak* self,const char* filename);
bool Pak_replaceFile(Pak* self,const char* filename,const char* fileData,uint32_t fileSize);
bool Pak_renameFile(Pak* self,const char* filename,const char* newFilename);
void Pak_extractFileByIndex(Pak* self,unsigned index,const char* outputFilename,bool convertADPCM,bool convertDEF);
bool Pak_extractFile(Pak* self,const char* filename,const char* outputFilename,bool convertADPCM,bool convertDEF);

#endif
