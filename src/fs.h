// KAO1Unpack
// Copyright (C) 2022 mrkubax10

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef KAO1UNPACK_FS_H
#define KAO1UNPACK_FS_H

#include <stdbool.h>
#include <stdio.h>

#if defined(__unix__)
	#define FOLDER_SEPARATOR '/'
#elif defined(_WIN32)
	#define FOLDER_SEPARATOR '\\'
#else
	#define FOLDER_SEPARATOR '/'
#endif

bool isFolder(const char* path);
char** listFolder(const char* path,unsigned* length);
void createFolder(const char* path);
void createFolders(const char* path);
void truncateFile(FILE* file,unsigned size);

#endif
