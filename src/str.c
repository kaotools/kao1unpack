// KAO1Unpack
// Copyright (C) 2022 mrkubax10

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "str.h"

#include <stdlib.h>
#include <string.h>


void stringReplace(char* str,char src,char dest){
	for(unsigned int i=0; i<strlen(str); i++){
		if(str[i]==src)
			str[i]=dest;
	}
}

void arrayFree(void** array,unsigned int len){
	for(unsigned int i=0; i<len; i++){
		free(array[i]);
	}
	free(array);
}
bool stringEndsWith(const char* str,const char* str2){
	if(strlen(str2)>strlen(str))
		return false;
	return strncmp(str+strlen(str)-strlen(str2),str2,strlen(str2))==0;
}
bool stringStartsWith(const char* str,const char* str2){
	if(strlen(str2)>strlen(str))
		return false;
	return strncmp(str,str2,strlen(str2))==0;
}
unsigned stringCharCount(const char* str,char ch){
	unsigned count=0;
	for(unsigned i=0; i<strlen(str); i++){
		if(str[i]==ch)
			count++;
	}
	return count;
}
