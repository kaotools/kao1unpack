// KAO1Unpack
// Copyright (C) 2023 mrkubax10

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "mode_list.h"

#include <stdio.h>

#include "pak.h"

void modeList(const char* input){
	Pak pak;
	Pak_init(&pak);
	if(!Pak_load(&pak,input))
		return;

	for(unsigned i=0; i<pak.fileCount; i++){
		printf("[%d/%d] %s (size: %d, offset: %d)\n",i+1,pak.fileCount,pak.fileHeaders[i].filename,pak.fileHeaders[i].size,pak.fileHeaders[i].offset);
	}

	Pak_delete(&pak);
}
