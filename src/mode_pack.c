// KAO1Unpack
// Copyright (C) 2023 mrkubax10

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "mode_pack.h"

#include <string.h>
#include <stdlib.h>

#include "pak.h"
#include "encoding.h"
#include "binary_utils.h"
#include "fs.h"
#include "str.h"

void modePack(const char* input,const char* output,bool convertADPCM,bool convertDEF,bool useWindowsPath){
	FILE* out=fopen(output,"wb");
	if(!out){
		printf("Failed to open '%s' for writing\n",output);
		return;
	}
	unsigned fileCount=0;
	char** folderEntries=listFolder(input,&fileCount);
	PakFile* fileHeaders=malloc(fileCount*sizeof(PakFile));
	uint32_t offset=0;
	char temp2[4];

	for(unsigned i=0; i<fileCount; i++){
		unsigned tempPathLength=strlen(input)+strlen(folderEntries[i])+1;
#if defined(_MSC_VER)
		char* temp=malloc(tempPathLength+1);
#else
		char temp[tempPathLength+1];
#endif
		snprintf(temp,tempPathLength+1,"%s/%s",input,folderEntries[i]);
		FILE* inp=fopen(temp,"rb");
		if(!inp){
			printf("Failed to open '%s' for reading\n",temp);
			continue;
		}
		PakFile* header=&fileHeaders[i];
		fseek(inp,0,SEEK_END);
		header->size=ftell(inp);
		rewind(inp);
		header->offset=offset;
		strncpy(header->filename,folderEntries[i],80);
		if(useWindowsPath)
			stringReplace(header->filename,'/','\\');
		else
			stringReplace(header->filename,'\\','/');
		char* data=malloc(header->size);
		fread(data,header->size,1,inp);
		fclose(inp);
		printf("[%d/%d] Writing '%s' at offset %d with size %d...\n",i+1,fileCount,temp,offset,header->size);
#if defined(_MSC_VER)
		free(temp);
#endif
		if((stringEndsWith(folderEntries[i],".def") || stringEndsWith(folderEntries[i],".at") || stringStartsWith(folderEntries[i],"text/localize.win/denis.") || stringEndsWith(folderEntries[i],"/scene.cut"))  && convertDEF)
			encodeDEFFile(data,header->size);
		if(stringEndsWith(input,".adp") && strncmp(data,"RIFF",4)==0 && convertADPCM){
			uint32_t newSize;
			char* convertedData=encodeADPCMFile(input,data,header->size,&newSize);
			if(convertedData){
				fwrite(convertedData,newSize,1,out);
				free(convertedData);
				header->size=newSize;
			}
			else
				fwrite(data,header->size,1,out);
		}
		else
			fwrite(data,header->size,1,out);
		free(data);
		offset+=header->size;
	}
	arrayFree((void**)folderEntries,fileCount);

	uint32_t fileTableOffset=ftell(out);
	for(unsigned i=0; i<fileCount; i++){
		PakFile* header=&fileHeaders[i];
		fwrite(header->filename,80,1,out);
		uint32ToBytes(header->offset,temp2);
		fwrite(temp2,4,1,out);
		uint32ToBytes(header->size,temp2);
		fwrite(temp2,4,1,out);
	}

	uint32ToBytes(fileCount,temp2);
	fwrite(temp2,4,1,out);
	uint32ToBytes(fileTableOffset,temp2);
	fwrite(temp2,4,1,out);
	fwrite("T8FM",4,1,out);

	fclose(out);
	free(fileHeaders);
}
