// KAO1Unpack
// Copyright (C) 2023 mrkubax10

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "mode_unpack_selected.h"

#include <string.h>

#include "pak.h"
#include "fs.h"
#include "str.h"

void modeUnpackSelected(const char* input,const char* output,const char* filenames,bool convertADPCM,bool convertDEF){
	Pak pak;
	Pak_init(&pak);
	if(!Pak_load(&pak,input))
		return;

	if(!strchr(filenames,';')){
		unsigned outputFilenameLength=strlen(output)+strlen(filenames)+1;
		char outputFilename[outputFilenameLength+1];
		char filenamesCopy[strlen(filenames)+1];
		strcpy(filenamesCopy,filenames);
		printf("Extracting file %s\n",filenames);
		stringReplace(filenamesCopy,'\\','/');
		snprintf(outputFilename,outputFilenameLength+1,"%s/%s",output,filenamesCopy);
		createFolders(outputFilename);
		Pak_extractFile(&pak,filenames,outputFilename,convertADPCM,convertDEF);
		Pak_delete(&pak);
		return;
	}

#if defined(_MSC_VER)
	char* filenamesCopy=malloc(strlen(filenames)+1);
#else
	char filenamesCopy[strlen(filenames)+1];
#endif
	strcpy(filenamesCopy,filenames);
	unsigned filenamesCopyLength=strlen(filenamesCopy);

	char* section=filenamesCopy;
	char* prevSection=filenamesCopy;
	while((section && (section=strchr(section,';'))) || (prevSection && prevSection!=filenamesCopy+filenamesCopyLength)){
		if(section)
			*section=0;
		unsigned outputFilenameLength=strlen(output)+strlen(prevSection)+1;
#if defined(_MSC_VER)
		char* outputFilename=malloc(outputFilenameLength+1);
		char* prevSectionCopy=malloc(strlen(prevSection)+1);
#else
		char outputFilename[outputFilenameLength+1];
		char prevSectionCopy[strlen(prevSection)+1];
#endif
		strcpy(prevSectionCopy,prevSection);
		printf("Extracting file %s\n",prevSection);
		stringReplace(prevSectionCopy,'\\','/');
		snprintf(outputFilename,outputFilenameLength+1,"%s/%s",output,prevSectionCopy);
		createFolders(outputFilename);
		Pak_extractFile(&pak,prevSection,outputFilename,convertADPCM,convertDEF);
#if defined(_MSC_VER)
		free(outputFilename);
		free(prevSectionCopy);
#endif
		if(section){
			section++;
			prevSection=section;
		}
		else if(prevSection)
			break;
	}
#if defined(_MSC_VER)
	free(filenamesCopy);
#endif

	Pak_delete(&pak);
}
