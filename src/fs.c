// KAO1Unpack
// Copyright (C) 2022 mrkubax10

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "fs.h"

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#if defined(__unix__)
	#include <dirent.h>
	#include <sys/stat.h>
	#include <unistd.h>
#elif defined(_WIN32)
	#define WIN32_LEAN_AND_MEAN
	#include <windows.h>
	#include <io.h>
#endif

#include "str.h"

// Microsoft...
#ifdef _WIN32
wchar_t* toWideString(char* str){
	wchar_t* wstr=malloc(strlen(str)*sizeof(wchar_t));
	mbstowcs(wstr,str,strlen(str));
	return wstr;
}
#ifdef _MSC_VER
#define WINAPI_STRING(str) toWideString(str)
#define WINAPI_STRING_FREE(str) free(str)
#else
#define WINAPI_STRING(str) str
#define WINAPI_STRING_FREE(str)
#endif
#endif

bool isFolder(const char* path){
#if defined(__unix__)
	struct stat fileStat;
	stat(path,&fileStat);
	return S_ISDIR(fileStat.st_mode);
#elif defined(_WIN32)
	void* convertedPath=WINAPI_STRING(path);
	DWORD fileAttributes=GetFileAttributes(convertedPath);
	WINAPI_STRING_FREE(convertedPath);
	return (fileAttributes&FILE_ATTRIBUTE_DIRECTORY);
#endif
}
char** listFolder(const char* path,unsigned* length){
#if defined(__unix__)
	DIR* d=opendir(path);
	if(!d)
		return 0;
	struct dirent* entry;
	unsigned pathLength=strlen(path);
	char** entries=0;
	while(entry=readdir(d)){
		if(strcmp(entry->d_name,".")==0 || strcmp(entry->d_name,"..")==0)
			continue;
		unsigned entryNameLength=strlen(entry->d_name);
		unsigned fullEntryPathLength=pathLength+entryNameLength+1;
		char* fullEntryPath=malloc(fullEntryPathLength+1);
		snprintf(fullEntryPath,fullEntryPathLength+1,"%s/%s",path,entry->d_name);
		if(isFolder(fullEntryPath)){
			unsigned subfolderEntryCount=0;
			char** subfolderEntries=listFolder(fullEntryPath,&subfolderEntryCount);
			if(!subfolderEntries)
				continue;
			entries=realloc(entries,((*length)+subfolderEntryCount)*sizeof(char*));
			for(unsigned i=0; i<subfolderEntryCount; i++){
				entries[(*length)+i]=subfolderEntries[i];
			}
			free(subfolderEntries);
			(*length)+=subfolderEntryCount;
		}
		else{
			char* entryName;
			if(stringCharCount(path,'/')==0){
				entryName=malloc(strlen(entry->d_name)+1);
				strcpy(entryName,entry->d_name);
			}
			else{
				char* baseFolder=strchr(path,'/')+1;
				unsigned entryNameLength=strlen(baseFolder)+strlen(entry->d_name)+1;
				entryName=malloc(entryNameLength+1);
				snprintf(entryName,entryNameLength+1,"%s/%s",baseFolder,entry->d_name);
			}
			(*length)++;
			entries=realloc(entries,(*length)*sizeof(char*));
			entries[(*length)-1]=entryName;
		}
		free(fullEntryPath);
	}
	closedir(d);
	return entries;
#elif defined(_WIN32)
	unsigned findPathLength=strlen(path)+2;
#if defined(_MSC_VER)
	char* findPath=malloc(findPathLength+1);
#else
	char findPath[findPathLength+1];
#endif
	snprintf(findPath,findPathLength+1,"%s\\*",path);
	WIN32_FIND_DATA findData;
	HANDLE find=FindFirstFile(findPath,&findData);
	unsigned pathLength=strlen(path);
	char** entries=0;
	if(find!=INVALID_HANDLE_VALUE){
		do{
			if(strcmp(findData.cFileName,".")==0 || strcmp(findData.cFileName,"..")==0)
				continue;
			unsigned entryLength=strlen(findData.cFileName);
			if(findData.dwFileAttributes && findData.dwFileAttributes&FILE_ATTRIBUTE_DIRECTORY){
				unsigned fullEntryPathLength=pathLength+entryLength+1;
#if defined(_MSC_VER)
				char* fullEntryPath=malloc(fullEntryPathLength+1);
#else
				char fullEntryPath[fullEntryPathLength+1];
#endif
				snprintf(fullEntryPath,fullEntryPathLength+1,"%s\\%s",path,findData.cFileName);
				unsigned subfolderEntryCount=0;
				char** subfolderEntries=listFolder(fullEntryPath,&subfolderEntryCount);
				if(!subfolderEntries)
					continue;
				entries=realloc(entries,((*length)+subfolderEntryCount)*sizeof(char*));
				for(unsigned i=0; i<subfolderEntryCount; i++){
					entries[(*length)+i]=subfolderEntries[i];
				}
				free(subfolderEntries);
				(*length)+=subfolderEntryCount;
#if defined(_MSC_VER)
				free(fullEntryPath);
#endif
			}
			else{
				char* entryName;
				if(strchr(path,'\\')){
					char* baseFolder=strchr(path,'\\')+1;
					unsigned entryNameLength=strlen(baseFolder)+entryNameLength;
					entryName=malloc(entryNameLength+1);
					snprintf(entryName,entryNameLength+1,"%s\\%s",baseFolder,findData.cFileName);
				}
				else{
					entryName=malloc(entryLength+1);
					strcpy(entryName,findData.cFileName);
				}
				(*length)++;
				entries=realloc(entries,(*length)*sizeof(char*));
				entries[(*length)-1]=entryName;
			}
		}
		while(FindNextFile(find,&findData));
		FindClose(find);
	}
#if defined(_MSC_VER)
	free(findPath);
#endif
	return entries;
#endif
}
void createFolder(const char* path){
#if defined(__unix__)
	mkdir(path,S_IRWXU);
#elif defined(_WIN32)
	void* convertedPath=WINAPI_STRING(path);
	CreateDirectory(convertedPath,0);
	WINAPI_STRING_FREE(convertedPath);
#endif
}
void createFolders(const char* path){
	if(!strchr(path,FOLDER_SEPARATOR)){
		createFolder(path);
		return;
	}
#if defined(_MSC_VER)
	char* cstr=malloc(strlen(path)+1);
#else
	char cstr[strlen(path)+1];
#endif
	strcpy(cstr,path);
	char* section=cstr;
	while(section=strchr(section,FOLDER_SEPARATOR)){
		*section=0;
		createFolder(cstr);
		*section=FOLDER_SEPARATOR;
		section++;
	}
#if defined(_MSC_VER)
	free(cstr);
#endif
}
void truncateFile(FILE* file,unsigned size){
#if defined(__unix__)
	ftruncate(fileno(file),size);
#elif defined(_WIN32)
	HANDLE fileHandle=(HANDLE)_get_osfhandle(_fileno(file));
	SetFileValidData(fileHandle,size);
	SetEndOfFile(fileHandle);
#endif
}
