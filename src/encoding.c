// KAO1Unpack
// Copyright (C) 2023 mrkubax10

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "encoding.h"

#include "adpcm.h"
#include "binary_utils.h"

#include <stdlib.h>
#include <string.h>

char* encodeADPCMFile(const char* filename,const char* data,uint32_t size,uint32_t* outputSize){
	int16_t wavChannels;
	int16_t wavBitsPerSample;
	int32_t wavDataSize;
	char temp[4],temp2[2];
	wavChannels=bytesToUint16(&data[22]);
	wavBitsPerSample=bytesToUint16(&data[34]);
	wavDataSize=bytesToUint32(&data[40]);
	*outputSize=wavDataSize/4+8;
	if(wavChannels!=2){
		printf("Failed to encode wav file %s: wav must be stereo\n",filename);
		return 0;
	}
	if(wavBitsPerSample!=16){
		printf("Failed to encode wav file %s: wav resolution must be 16 bits\n",filename);
		return 0;
	}
	// Split interleaved stereo into two channels
	int16_t* wavChannel1=malloc(wavDataSize/2);
	int16_t* wavChannel2=malloc(wavDataSize/2);
	for(unsigned i=0; i<(unsigned)wavDataSize/4; i++){
		wavChannel1[i]=(int16_t)bytesToUint16(&data[i*4+44]);
		wavChannel2[i]=(int16_t)bytesToUint16(&data[i*4+46]);
	}
	int8_t* adpcmChannel1=encodeADPCM(wavChannel1,wavDataSize/4);
	int8_t* adpcmChannel2=encodeADPCM(wavChannel2,wavDataSize/4);
	free(wavChannel1);
	free(wavChannel2);
	// Write adp header
	char* output=malloc(*outputSize);
	strncpy(output,"tadp",4);
	int32_t adpcmChannelSize=wavDataSize/2/4;
	uint32ToBytes(adpcmChannelSize,temp);
	memcpy(&output[4],temp,4);
	memcpy(&output[8],adpcmChannel1,adpcmChannelSize);
	memcpy(&output[adpcmChannelSize+8],adpcmChannel2,adpcmChannelSize);
	free(adpcmChannel1);
	free(adpcmChannel2);
	return output;
}
void encodeDEFFile(char* data,uint32_t size){
	const uint8_t str[]={'t','a','t','e'};
	uint8_t index=0;
	uint8_t decoder=0x12;
	for(uint32_t i=0; i<size; i++){
		decoder=(decoder+data[i])^str[index];
		data[i]=decoder;
		index++;
		index%=4;
	}
}
