// KAO1Unpack
// Copyright (C) 2022 mrkubax10

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "adpcm.h"

#include <stdlib.h>

static const int adpcmIndexTable[]={
	-1, -1, -1, -1, 2, 4, 6, 8,
	-1, -1, -1, -1, 2, 4, 6, 8
};
static const int adpcmStepTable[]={ 
	7, 8, 9, 10, 11, 12, 13, 14, 16, 17,
	19, 21, 23, 25, 28, 31, 34, 37, 41, 45,
	50, 55, 60, 66, 73, 80, 88, 97, 107, 118,
	130, 143, 157, 173, 190, 209, 230, 253, 279, 307,
	337, 371, 408, 449, 494, 544, 598, 658, 724, 796,
	876, 963, 1060, 1166, 1282, 1411, 1552, 1707, 1878, 2066,
	2272, 2499, 2749, 3024, 3327, 3660, 4026, 4428, 4871, 5358,
	5894, 6484, 7132, 7845, 8630, 9493, 10442, 11487, 12635, 13899,
	15289, 16818, 18500, 20350, 22385, 24623, 27086, 29794, 32767
};
static int16_t decodeADPCMSample(int8_t nibble,int* stepIndex,int* step,int* predictor){
	(*stepIndex)+=adpcmIndexTable[(uint8_t)nibble];
	(*stepIndex)=(*stepIndex)>88?88:(*stepIndex);
	(*stepIndex)=(*stepIndex)<0?0:(*stepIndex);
	int8_t delta=nibble&7;
	int diff=(*step)>>3;
	if(delta&4)
		diff+=(*step);
	if(delta&2)
		diff+=((*step)>>1);
	if(delta&1)
		diff+=((*step)>>2);
	if(nibble&8)
		(*predictor)-=diff;
	else
		(*predictor)+=diff;
	(*predictor)=(*predictor)>32767?32767:(*predictor);
	(*predictor)=(*predictor)<-32768?-32768:(*predictor);
	(*step)=adpcmStepTable[(*stepIndex)];
	return *predictor;
}
int16_t* decodeADPCM(char* data,unsigned int size){
	int stepIndex=0;
	int step=adpcmStepTable[stepIndex];
	int predictor=0;
	int16_t* output=malloc(size*4);
	for(unsigned int i=0; i<size; i++){
		int8_t n1=data[i]&0x0F;
		int8_t n2=(data[i]&0xF0)>>4;
		output[i*2]=decodeADPCMSample(n1,&stepIndex,&step,&predictor);
		output[i*2+1]=decodeADPCMSample(n2,&stepIndex,&step,&predictor);
	}
	return output;
}
static int8_t encodeADPCMSample(int16_t pcm,int* stepIndex,int* step,int* predictor){
	int diff=pcm-(*predictor);
	int output;
	if(diff>=0)
		output=0;
	else{
		output=8;
		diff=-diff;
	}
	if(diff>=(*step)){
		output|=4;
		diff-=(*step);
	}
	int tempstep=((*step)>>1);
	if(diff>=tempstep){
		output|=2;
		diff-=tempstep;
	}
	tempstep>>=1;
	if(diff>=tempstep)
		output|=1;
	int quantizedDiff=(*step)>>3;
	if(output&4)
		quantizedDiff+=(*step);
	if(output&2)
		quantizedDiff+=((*step)>>1);
	if(output&1)
		quantizedDiff+=((*step)>>2);
	if(output&8)
		(*predictor)-=quantizedDiff;
	else
		(*predictor)+=quantizedDiff;
	(*predictor)=(*predictor)>32767?32767:(*predictor);
	(*predictor)=(*predictor)<-32768?-32768:(*predictor);
	(*stepIndex)+=adpcmIndexTable[output];
	(*stepIndex)=(*stepIndex)>88?88:(*stepIndex);
	(*stepIndex)=(*stepIndex)<0?0:(*stepIndex);
	(*step)=adpcmStepTable[(*stepIndex)];
	return (output&0x0F);			
}
int8_t* encodeADPCM(int16_t* data,unsigned size){
	int stepIndex=0;
	int step=adpcmStepTable[stepIndex];
	int predictor=0;
	int8_t* output=malloc(size/2);
	for(unsigned i=0; i<size/2; i++){
		int8_t samples=(encodeADPCMSample(data[i*2+1],&stepIndex,&step,&predictor)<<4);
		samples|=encodeADPCMSample(data[i*2],&stepIndex,&step,&predictor);
		output[i]=samples;
	}
	return output;
}
