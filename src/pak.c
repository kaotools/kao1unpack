// KAO1Unpack
// Copyright (C) 2022 mrkubax10
// Thanks to Flower35 for PAK documentation: https://drive.google.com/file/d/1jloDqtUak-9OhOQ6Ayflxj2rdGT5HqZq/view

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "pak.h"

#include <string.h>
#include <stdlib.h>

#include "adpcm.h"
#include "binary_utils.h"
#include "str.h"

void Pak_init(Pak* self){
	self->fileHeaders=0;
	self->fileCount=0;
	self->contentSize=0;
	self->size=0;
	self->file=0;
}
void Pak_delete(Pak* self){
	if(self->fileHeaders)
		free(self->fileHeaders);
	if(self->file)
		fclose(self->file);
}
PakFile* Pak_lookupFile(Pak* self,const char* filename,unsigned* index){
	for(unsigned i=0; i<self->fileCount; i++){
		if(strcmp(self->fileHeaders[i].filename,filename)==0){
			if(index)
				*index=i;
			return &self->fileHeaders[i];
		}
	}
	return 0;
}
bool Pak_load(Pak* self,const char* path){
	self->file=fopen(path,"rb+");
	if(!self->file){
		printf("Failed to open file %s\n",path);
		return false;
	}
	if(!Pak_loadEntryTable(self,0))
		return false;
	return true;
}
bool Pak_loadEntryTable(Pak* self,const char* path){
	fseek(self->file,0,SEEK_END);
	self->size=ftell(self->file);
	fseek(self->file,self->size-12,SEEK_SET);
	char footer[12];
	fread(footer,12,1,self->file);
	self->fileCount=bytesToUint32(footer);
	self->contentSize=bytesToUint32(&footer[4]);
	uint8_t str[]={'T','8','F','M'};
	if(memcmp(&footer[8],str,4)!=0){
		printf("Invalid file: %s\n",path);
		fclose(self->file);
		return false;
	}
	self->fileHeaders=malloc(self->fileCount*sizeof(PakFile));
	char temp[4];
	for(unsigned i=0; i<self->fileCount; i++){
		fseek(self->file,self->contentSize+i*sizeof(PakFile),SEEK_SET);
		fread((uint8_t*)&self->fileHeaders[i].filename,80,1,self->file);
		fread(temp,4,1,self->file);
		self->fileHeaders[i].offset=bytesToUint32(temp);
		fread(temp,4,1,self->file);
		self->fileHeaders[i].size=bytesToUint32(temp);
	}
	return true;
}
char* Pak_getFile(Pak* self,const char* filename,uint32_t* size){
	PakFile* header=Pak_lookupFile(self,filename,0);
	if(!header)
		return 0;
	fseek(self->file,header->offset,SEEK_SET);
	if(size)
		*size=header->size;
	char* output=malloc(header->size);
	fread(output,header->size,1,self->file);
	return output;
}
char* Pak_getFileByIndex(Pak* self,unsigned index,uint32_t* size){
	PakFile* header=&self->fileHeaders[index];
	fseek(self->file,header->offset,SEEK_SET);
	if(size)
		*size=header->size;
	char* output=malloc(header->size);
	fread(output,header->size,1,self->file);
	return output;
}
static void Pak_writeFileTable(Pak* self){
	char temp[4];
	fseek(self->file,self->contentSize,SEEK_SET);
	for(uint32_t i=0; i<self->fileCount; i++){
		fwrite(self->fileHeaders[i].filename,80,1,self->file);
		uint32ToBytes(self->fileHeaders[i].offset,temp);
		fwrite(temp,4,1,self->file);
		uint32ToBytes(self->fileHeaders[i].size,temp);
		fwrite(temp,4,1,self->file);
	}
}
static void Pak_writeFooter(Pak* self){
	char temp[4];
	fseek(self->file,self->size-12,SEEK_SET);
	uint32ToBytes(self->fileCount,temp);
	fwrite(temp,4,1,self->file);
	uint32ToBytes(self->contentSize,temp);
	fwrite(temp,4,1,self->file);
	fwrite("T8FM",4,1,self->file);
}
void Pak_addFile(Pak* self,const char* filename,const char* fileData,uint32_t fileSize){
	if(Pak_lookupFile(self,filename,0))
		return;

	fseek(self->file,self->contentSize,SEEK_SET);
	fwrite(fileData,fileSize,1,self->file);

	self->fileCount++;
	self->fileHeaders=realloc(self->fileHeaders,self->fileCount*sizeof(PakFile));
	strncpy(self->fileHeaders[self->fileCount-1].filename,filename,80);
	self->fileHeaders[self->fileCount-1].offset=self->contentSize;
	self->fileHeaders[self->fileCount-1].size=fileSize;
	self->contentSize+=fileSize;
	self->size=self->contentSize+self->fileCount*sizeof(PakFile)+12;

	Pak_writeFileTable(self);
	Pak_writeFooter(self);
}
bool Pak_deleteFile(Pak* self,const char* filename){
	unsigned index;
	PakFile* header=Pak_lookupFile(self,filename,&index);
	if(!header)
		return false;

	if(index<self->fileCount-1){
		fseek(self->file,header->offset+header->size,SEEK_SET);
		uint32_t remainingSize=self->contentSize-header->offset-header->size;
		char* remaining=malloc(remainingSize);
		fread(remaining,remainingSize,1,self->file);
		fseek(self->file,header->offset,SEEK_SET);
		fwrite(remaining,remainingSize,1,self->file);
		for(uint32_t i=index+1; i<self->fileCount; i++){
			self->fileHeaders[i].offset-=header->size;
			self->fileHeaders[i-1]=self->fileHeaders[i];
		}
	}
	self->contentSize-=header->size;
	self->fileCount--;
	self->fileHeaders=realloc(self->fileHeaders,self->fileCount*sizeof(PakFile));
	self->size=self->contentSize+self->fileCount*sizeof(PakFile)+12;

	Pak_writeFileTable(self);
	Pak_writeFooter(self);
	truncateFile(self->file,self->size);

	return true;
}
bool Pak_replaceFile(Pak* self,const char* filename,const char* fileData,uint32_t fileSize){
	unsigned index;
	PakFile* header=Pak_lookupFile(self,filename,&index);
	if(!header)
		return false;

	if(header->size==fileSize){
		fseek(self->file,header->offset,SEEK_SET);
		fwrite(fileData,fileSize,1,self->file);
	}
	else{
		int32_t sizeDiff=(int32_t)fileSize-(int32_t)header->size;

		if(index<self->fileCount-1){
			fseek(self->file,header->offset+header->size,SEEK_SET);
			uint32_t remainingSize=self->contentSize-header->offset-header->size;
			char* remaining=malloc(remainingSize);
			fread(remaining,remainingSize,1,self->file);
			fseek(self->file,header->offset,SEEK_SET);
			fwrite(fileData,fileSize,1,self->file);
			fwrite(remaining,remainingSize,1,self->file);
			free(remaining);
		}
		else{
			fseek(self->file,header->offset,SEEK_SET);
			fwrite(fileData,fileSize,1,self->file);
		}

		for(unsigned i=index+1; i<self->fileCount; i++){
			self->fileHeaders[i].offset+=sizeDiff;
		}
		header->size=fileSize;
		self->contentSize+=sizeDiff;
		self->size=self->contentSize+self->fileCount*sizeof(PakFile)+12;

		Pak_writeFileTable(self);
		Pak_writeFooter(self);
		truncateFile(self->file,self->size);
	}
}
bool Pak_renameFile(Pak* self,const char* filename,const char* newFilename){
	unsigned index;
	PakFile* header=Pak_lookupFile(self,filename,&index);
	if(!header)
		return false;

	strncpy(header->filename,newFilename,80);
	fseek(self->file,self->contentSize+index*sizeof(PakFile),SEEK_SET);
	fwrite(header->filename,80,1,self->file);

	return true;
}
void Pak_extractFileByIndex(Pak* self,unsigned index,const char* outputFilename,bool convertADPCM,bool convertDEF){
	PakFile* header=&self->fileHeaders[index];
	
	FILE* output=fopen(outputFilename,"wb");
	if(!output){
		printf("(Warning) [Pak] Failed to open '%s' for writing\n",outputFilename);
		return;
	}
	char* data=malloc(header->size);
	fseek(self->file,header->offset,SEEK_SET);
	fread(data,header->size,1,self->file);

	if((stringEndsWith(header->filename,".def") || stringEndsWith(header->filename,".at") || stringStartsWith(header->filename,"text/localize.win/denis.") || stringEndsWith(header->filename,"/scene.cut"))  && convertDEF){
		const uint8_t str[]={'t','a','t','e'};
		uint8_t temp=0;
		uint8_t ind=0;
		uint8_t decoder=0x12;
		for(unsigned i=0; i<header->size; i++){
			temp=(str[ind]^data[i])-decoder;
			decoder=data[i];
			data[i]=temp;
			ind++;
			ind%=4;
		}
	}
	if(stringEndsWith(header->filename,".adp") && strncmp(data,"tadp",4)==0 && convertADPCM){
		char temp[4],temp2[2];
		uint32_t channelLength=bytesToUint32(&data[4]);
		int16_t* wavChannel1=decodeADPCM(data+8,header->size-8-channelLength);
		int16_t* wavChannel2=decodeADPCM(data+8+channelLength,header->size-8-channelLength);
		uint16_t wavChannels=2;
		uint32_t wavFileSize=channelLength*4*wavChannels+36;
		uint32_t wavFormatSize=16;
		uint16_t wavType=1;
		uint32_t wavSampleRate=44100;
		uint32_t wavBytesSecond=(wavSampleRate*wavFormatSize*wavChannels)/8;
		uint16_t wavBytesSample=(wavChannels*wavFormatSize)/8;
		uint16_t wavRes=16;
		uint32_t wavDataSize=channelLength*4*wavChannels;
		fwrite("RIFF",4,1,output);
		uint32ToBytes(wavFileSize,temp);
		fwrite(temp,4,1,output);
		fwrite("WAVEfmt ",8,1,output);
		uint32ToBytes(wavFormatSize,temp);
		fwrite(temp,4,1,output);
		uint16ToBytes(wavType,temp2);
		fwrite(temp2,2,1,output);
		uint16ToBytes(wavChannels,temp2);
		fwrite(temp2,2,1,output);
		uint32ToBytes(wavSampleRate,temp);
		fwrite(temp,4,1,output);
		uint32ToBytes(wavBytesSecond,temp);
		fwrite(temp,4,1,output);
		uint16ToBytes(wavBytesSample,temp2);
		fwrite(temp2,2,1,output);
		uint16ToBytes(wavRes,temp2);
		fwrite(temp2,2,1,output);
		fwrite("data",4,1,output);
		uint32ToBytes(wavDataSize,temp);
		fwrite(temp,4,1,output);
		int16_t* wavChannel=malloc((header->size-8)*2*sizeof(int16_t));
		for(unsigned i=0; i<header->size-8-channelLength; i++){
			wavChannel[i*4]=wavChannel1[i*2];
			wavChannel[i*4+1]=wavChannel2[i*2];
			wavChannel[i*4+2]=wavChannel1[i*2+1];
			wavChannel[i*4+3]=wavChannel2[i*2+1];
		}
		free(wavChannel1);
		free(wavChannel2);
		fwrite(wavChannel,channelLength*4*sizeof(int16_t),1,output);
		free(wavChannel);
	}
	else
		fwrite(data,header->size,1,output);

	fclose(output);
	free(data);
}
bool Pak_extractFile(Pak* self,const char* filename,const char* outputFilename,bool convertADPCM,bool convertDEF){
	unsigned index;
	if(!Pak_lookupFile(self,filename,&index))
		return false;
	Pak_extractFileByIndex(self,index,outputFilename,convertADPCM,convertDEF);
	return true;
}
