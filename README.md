# Kao1Unpack
Utility to unpack and create Kao 1 .PAK archives
## Running
You need to have C compiler and CMake installed.  
```console
$ mkdir build
$ cd build
$ cmake ..
$ make -j$(nproc)
```
## Usage
```console
$ ./KAO1Unpack <arguments...>
```
Use `--help` argument to see more info
## Project status
Features:
- Archive unpacking
- Archive creating
- Archive listing
- Renaming files in archive
- Adding files to archive
- Deleting files from archive
- Replacing files in archive
- Unpacking selected files from archive
- Showing archive statistics
- DEF script decoding
- DEF script encoding
- ADPCM decoding
- ADPCM encoding
